# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'json'
require 'open-uri'

Pokemon.destroy_all
Type.destroy_all
PokemonType.destroy_all

POKE_URL = 'https://pokeapi.co/api/v2/pokemon/?limit=151'
TYPE_URL = 'https://pokeapi.co/api/v2/type/?limit=18'

poke_data = JSON.parse(open(POKE_URL).read)
pokemon = poke_data["results"]

pokemon.each do |poke|
    Pokemon.create!(poke)
end

type_data = JSON.parse(open(TYPE_URL).read)
types = type_data["results"]

types.each do |type|
    Type.create!(type)
end

pokemon_types = []
pokemon.each do |poke|
    details = JSON.parse(open(poke["url"]).read)
    poke_id = Pokemon.find_by(name: details["name"])["id"]
    type1_id = Type.find_by(name: details["types"][0]["type"]["name"])["id"]
    if details["types"].length == 2
        pokemon_types << {pokemon_id: poke_id, type_id: type1_id}
        type2_id = Type.find_by(name: details["types"][1]["type"]["name"])["id"]
        pokemon_types << {pokemon_id: poke_id, type_id: type2_id}
    else
        pokemon_types << {pokemon_id: poke_id, type_id: type1_id}
    end
end

pokemon_types.each do |poke_type|
    PokemonType.create!(poke_type)
end