module Types
    class TypingType < BaseObject
        field :id, ID, null: false
        field :name, String, null: false
        field :url, String, null: false
        field :pokemons, [PokemonType], null: false
    end
end