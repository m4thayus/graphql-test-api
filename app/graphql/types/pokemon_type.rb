module Types
    class PokemonType < BaseObject
        field :id, ID, null: false
        field :name, String, null: false
        field :url, String, null: false
        field :typing, [TypingType], null: false, method: :types
    end
end