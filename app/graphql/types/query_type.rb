module Types
    class QueryType < Types::BaseObject
        # Add root-level fields here.
        # They will be entry points for queries on your schema.

        field :pokemon, [PokemonType], null: false,
            description: "Show all 151 pokemon" do
                argument :name, String, required: false
            end
        def pokemon(name: nil)
            if name
                poke = Pokemon.find_by(name: name)
                [poke]
            else
                Pokemon.all
            end
        end

        field :typing, [TypingType], null: true,
            description: "Show all 18 types" do
                argument :name, String, required: false
            end
        def typing(name: nil)
            if name
                type = Type.find_by(name: name)
                [type]
            else
                Type.all
            end
        end

    end
end
